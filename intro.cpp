// TP 3? - MO2D
// 
#include <iostream>			// cout
#include <cstdlib>			// exit
#include <cmath>			// fabs
#include <GL/glut.h>
#include "Teclado.h"
#include "uglyfont.h"
#include <string>			// std::string, std::to_string
#include <fstream>			// file io

using namespace std;

// ------------------------------------------------------------
// variables globales
int
  w = 1024, h = 768;							// tamanio inicial de la ventana

double 
	AvionX = 400, 
	AvionY = 300, 
	AvionAng = 0, 
	ArmaAng = 0,
	ArmaTamanio = 0,
	DesplazamientoZ = 0.1;
int ammo = 750;
double fuel = 450;

const double PI=4*atan(1.0);
bool cl_info=true;								// informa por la linea de comandos
bool keyStates[256] = { false };				// Array con las teclas para saber sus estados

GLuint texid[2];




// ------------------------------------------------------------
// Textura

// Carga una imagen en formato ppm, crea el canal alpha (ya que las imagenes ppm 
// no guardan este canal) , crea los mipmaps y deja la textura lista para usar en nuestro programa
bool mipmap_ppm(const char *ifile){
	// Se declaran algunas variables para usar despues y se abre el archivo que contiene la imagen
	char dummy; int maxc, wt, ht;
	ifstream fileinput(ifile, ios::binary);
	// Si no se pudo abrir el archivo se escribe por consola que hubo un error y se anula la operacion
	if (!fileinput.is_open()) { cerr << "Not found" << endl; return false; }
	fileinput.get(dummy);
	// Se leen dos caracteres llamados "n�meros m�gicos" que indican que el archivo es una 
	// imagen en formato ppm. Estos caracteres son una 'P' seguida de un '6'
	// Si no se encuentran estos caracteres significa que el archivo no es una 
	// imagen ppm por lo que se anula el proceso
	if (dummy != 'P') { cerr << "Not P6 PPM file" << endl; return false; }
	fileinput.get(dummy);
	if (dummy != '6') { cerr << "Not P6 PPM file" << endl; return false; }
	// Se leen algunos caracteres de relleno, que no tienen ningun significado util, se leen solo 
	// para saltearlos. Primero se lee un espacio, luego, el metodo peek lee un caracter sin 
	// adelantar la posicion, si este es un '#' se continua leyendo hasta que se encuentra 
	// un salto de linea (un enter)  
	fileinput.get(dummy);
	dummy = fileinput.peek();
	if (dummy == '#') do {
		fileinput.get(dummy);
	} while (dummy != 10);
	// Se lee el ancho y alto de la imagen, y el ultimo indica la cantidad de colores de la imagen
	fileinput >> wt >> ht;
	fileinput >> maxc;
	// Se saltea un espacio en blanco
	fileinput.get(dummy);
	// Se reserva memoria para wt*xt pixels. Como tenemos un byte para cada canal (rgb), 
	// son 3 bytes por pixel. Por ultimo se cierra en archivo    
	unsigned char *img = new unsigned char[3 * wt*ht];
	fileinput.read((char *)img, 3 * wt*ht);
	fileinput.close();
	// gluBuild2DMipmaps(GL_TEXTURE_2D, 3, wt, ht,  GL_RGB, GL_UNSIGNED_BYTE, img);
	// Conversion a rgba alpha=255-(r+g+b)/3 (blanco=transparente, negro=opaco)
	// Las imagenes en formato ppm no pueden guardar el canal alpha, por lo que lo creamos 
	// a partir de los canales rgb. Primero reservamos memoria para wt*ht pixels, esta vez 
	// con 4 bytes por pixel (rgba)
	// Recorremos la imagen leida del archivo, copiamos los canales r, g y b. El canal 
	// alpha lo definimos como trasparente si el color es blanco (si r=g=b=255, 
	// entonces r+g+b=765). Sino alpha es completamente opaco (=255)
	unsigned char *imga = new unsigned char[4 * wt*ht];
	unsigned char r, g, b;
	for (int i = 0; i<wt*ht; i++){
		r = imga[4 * i + 0] = img[3 * i + 0];
		g = imga[4 * i + 1] = img[3 * i + 1];
		b = imga[4 * i + 2] = img[3 * i + 2];
		imga[4 * i + 3] = ((r + g + b == 765) ? 0 : 255);
	}
	// Por ultimo le pedimos a glut que cree un mipmap a partir de la imagen rgba y 
	// liberamos la memoria reservada. Al creer el mipmap la textura queda lista para usarse
	// Cualquier primitiva de dibujo que aparezca a continuacion, si esta habilitada la 
	// generacion de texturas 2D (con glEnable(GL_TEXTURE_2D)) hara uso de esta textura
	delete[] img;
	gluBuild2DMipmaps(GL_TEXTURE_2D, 4, wt, ht, GL_RGBA, GL_UNSIGNED_BYTE, imga);
	delete[] imga;
	return true;
}

// ------------------------------------------------------------
// Renderiza texto en pantalla usando UglyFont
void print_text(string cadena, float x, float y, float escala = 1.0, float r = 1.0, float g = 1.0, float b = 1.0, float a = 1.0, float angulo = 0.0, int centrado = 0, float width = 1.0) {
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glLineWidth(width);							// ancho de linea del caracter
	glColor4f(r, g, b, a);						// color del caracter
	glPushMatrix();
	glTranslatef(x, y, 0.5);					// posicion del caracter
	glScalef(escala, escala, escala);			// escala del caracter
	glRotatef(angulo, 0, 0, 1);					// angulo del caracter
	YsDrawUglyFont(cadena.c_str(), centrado);	// el caracter puede estar centrado o no
	glPopMatrix();
	glPopAttrib();
};

void DibujarCabina() {
  glColor3d(0.6,0.7,0.9);
  glBegin(GL_TRIANGLE_FAN);
  glVertex3d(0, 0, 0.1);
  for(double r = 0; r < PI * 2; r += 0.1)
	  glVertex3d(cos(r), sin(r), 0.1);
  glVertex3d(1, 0, 0.1);
  glEnd();
};
// Luz en la trompa del avion
void DibujarLuz() {
	glEnable(GL_BLEND);							// Para lograr el disfuminado de color
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	
	glColor4f(1.0, 1.0, 0.0, 0.0);				// Color Final
	glBegin(GL_QUADS);
		glVertex2d(-10, 23);
		glVertex2d(10, 23);
		glColor4f(1.0, 1.0, 1.0, 1.0);			// Color inicial
		glVertex2d(-0.0, 6);
		glVertex2d(0.0, 6);
	glEnd();
};

void DibujarCuerpo() {
  glColor3d(0.4,0.4,0.4);
  glBegin(GL_TRIANGLE_FAN);
	  glVertex2d(0.0, 0.0);
	  glVertex2d(0.0, 70.0);
	  glVertex2d(-8, 35.0);
	  glVertex2d(-10, -30.0);
	  glVertex2d(0.0, -15.0);
	  glVertex2d(10, -30.0);
	  glVertex2d(8, 35.0);
	  glVertex2d(0.0, 70.0);
  glEnd();
};

void DibujarAla() {
  glColor3d(0.7,0.7,0.7);
  glBegin(GL_TRIANGLE_FAN);
	  glVertex2d(35,10);
	  glVertex2d(0.0,20.0); 
	  glVertex2d(0.0,0.0); 
	  glVertex2d(35,4);
	  glVertex2d(50.0,0.0);
  glEnd();
};

// Arma bajo las alas 
void DibujarArma() {
	glColor3d(0.8, 0.2, 0.2);
	glBegin(GL_QUADS);
		glVertex2d(20.0, 20.0);
		glVertex2d(20.0, -12.0);
		glVertex2d(27.0, -12.0);
		glVertex2d(27.0, 20.0);
	glEnd();
};

void DibujarAvion() {
  glPushMatrix();									// Inicio push1
	  // Posiciona y rota el Avion en el modelo
	  glTranslated(AvionX, AvionY, DesplazamientoZ);
	  glRotated(AvionAng,0,0,1);  
	  // Dibujamos las distintas partes de la nave, aplicando las transformaciones necesarias
	  //Cuerpo
	  DibujarCuerpo();
	  // Ala derecha
	  glPushMatrix();
		  DibujarAla();
		  DibujarArma();
	  glPopMatrix();
	  // Ala izquierda
	  glPushMatrix();
		  glScaled(-1,1,1);							// Con este escalamiento logramos reflejar (x = -AnchoAla * x)  
		  DibujarAla();
		  DibujarArma();
	  glPopMatrix();
	  //Cabina
	  glPushMatrix();
		  glScaled(6,12,1);
		  DibujarLuz();
		  DibujarCabina();
	  glPopMatrix();
  glPopMatrix();									// Fin push1
};

void DibujarPared() {
  glColor3f(0.9f,0.9f,0.9f);
  glLineWidth(5.0);
  glBegin(GL_LINES);
	  glVertex3f(325, 400, -0.2f); glVertex3f(325, 200, -0.2f);
	  glVertex3f(325, 200, -0.2f); glVertex3f(475, 200, -0.2f);
	  glVertex3f(475, 200, -0.2f); glVertex3f(475, 400, -0.2f);
  glEnd();
};

// Dibuja unas lineas simulando el techo del hangar
void DibujaTecho () {
	glColor3f(0.6f, 0.5f, 0.5f);
	glLineWidth(1.5);
	glBegin(GL_LINES);
		glVertex3f(325, 390, -0.15f); glVertex3f(475, 390, -0.15f);
		glVertex3f(325, 370, -0.15f); glVertex3f(475, 370, -0.15f);
		glVertex3f(325, 350, -0.15f); glVertex3f(475, 350, -0.15f);
		glVertex3f(325, 330, -0.15f); glVertex3f(475, 330, -0.15f);
		glVertex3f(325, 310, -0.15f); glVertex3f(475, 310, -0.15f);
		glVertex3f(325, 290, -0.15f); glVertex3f(475, 290, -0.15f);
		glVertex3f(325, 270, -0.15f); glVertex3f(475, 270, -0.15f);
		glVertex3f(325, 250, -0.15f); glVertex3f(475, 250, -0.15f);
		glVertex3f(325, 230, -0.15f); glVertex3f(475, 230, -0.15f);
		glVertex3f(325, 210, -0.15f); glVertex3f(475, 210, -0.15f);
	glEnd();
};

void DibujaStats() {
	//						x		y		scale	r		g		b		a		angle	center	width
	print_text("Fuel: " + to_string((int)fuel),	850,	720,	10.0,	1.0,	0.0,	0.0,	1.0,	0,		0,		1.5);
	print_text("Ammo: " + to_string(ammo),	850,	700,	10.0,	1.0,	0.0,	0.0,	1.0,	0,		0,		1.5);
	glBegin(GL_QUADS);
		glColor4f(0.7f, 0.7f, 0.9f, 0.3f);
		glVertex3f(800, 750, 0.9f);
		glVertex3f(1000, 750, 0.9f);
		// Disfuminado hacia abajo
		glColor4f(0.7f, 0.7f, 0.9f, 0.1f);
		glVertex3f(1000, 680, 0.9f);
		glVertex3f(800, 680, 0.9f);
	glEnd();
};

void DibujaXYZ () {
	print_text("X: " + to_string(int(AvionX)), 10.0f, h - 20.0f, 10.0, 1.0, 0.0, 0.0, 1.0, 0, 0, 1.5);	// Muestra el texto con la posicion X
	print_text("Y: " + to_string(int(AvionY)), 10.0f, h - 40.0f, 10.0, 1.0, 0.0, 0.0, 1.0, 0, 0, 1.5);	// Muestra el texto con la posicion Y
	print_text("Z: " + to_string(float(DesplazamientoZ)), 10.0f, h - 60.0f, 10.0, 1.0, 0.0, 0.0, 1.0, 0, 0, 1.5);	// Muestra el texto con la posicion Z
	glBegin(GL_QUADS);
		glColor4f(0.7f, 0.9f, 0.3f, 0.3f);
		glVertex3f(3, 765, 0.9f);
		glVertex3f(140, 765, 0.9f);
		glVertex3f(140, 690, 0.9f);
		glVertex3f(3, 690, 0.9f);
	glEnd();
	glLineWidth(4.0f);											// Hace las lineas mas gruesas
	glBegin(GL_LINES);
		glColor4f(0.1f, 0.1f, 0.1f, 1.0f);
		glVertex3f(3, 765, 0.9f); glVertex3f(140, 765, 0.9f);	// Lineas horizontales
		glVertex3f(3, 690, 0.9f); glVertex3f(140, 690, 0.9f);
		glVertex3f(3, 765, 0.9f); glVertex3f(3, 690, 0.9f);		// Lineas verticales
		glVertex3f(140, 765, 0.9f); glVertex3f(140, 690, 0.9f);
	glEnd();
};

// Dibuja una nube por encima de todo
void DibujarNube() {			
	print_text("Nube", 730.0f, 430.0f, 10.0, 1.0, 1.0, 1.0, 1.0, 0, 0, 1.0f);
	glColor4f(0.7f, 0.7f, 0.9f, 0.7f);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texid[1]);
	glBegin(GL_QUADS);
		glTexCoord2f(0, 0); glVertex3f(700, 500, 0.9f);
		glTexCoord2f(3, 0);	glVertex3f(1000, 500, 0.9f);
		glTexCoord2f(3, 3);	glVertex3f(1000, 200, 0.9f);
		glTexCoord2f(0, 3); glVertex3f(700, 200, 0.9f);
	glEnd();
	glDisable(GL_TEXTURE_2D);
};

// Dibuja una textura de pasto en el suelo
void DibujarPiso(){
	glColor3f(0.5f, 0.9f, 0.5f);						// Cambio el color para hacer el pasto un poco mas verde
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texid[0]);				// Selecciono la textura a utilizar
	glBegin(GL_QUADS);
		glTexCoord2f(0, 0); glVertex3f(0, 0, -0.9f);
		glTexCoord2f(3, 0);	glVertex3f(1024, 0, -0.9f);
		glTexCoord2f(3, 3);	glVertex3f(1024, 768, -0.9f);
		glTexCoord2f(0, 3); glVertex3f(0, 768, -0.9f);
	glEnd();
	glDisable(GL_TEXTURE_2D);
};

// ------------------------ Teclado ------------------------ //
void keyPressed(unsigned char key, int x, int y) {
	keyStates[key] = true;						// Set the state of the current key to pressed  
};

void keyUp(unsigned char key, int x, int y) {
	keyStates[key] = false;						// Set the state of the current key to not pressed  
};

void keyOperations() {
	// Convierte AvionAng de grados a radianes
	double ang = AvionAng*PI / 180.0;
	if (keyStates['w'] && fuel > 0 || keyStates['W'] && fuel > 0) {
		// double sin(double ang); // Calcula el seno de ang medido en radianes
		AvionX -= 0.05f * sin(ang);
		AvionY += 0.05f * cos(ang);
		fuel -= 0.01;
		glutPostRedisplay();
	}
	if (keyStates['s'] && fuel > 0 || keyStates['S'] && fuel > 0) {
		AvionX += 0.05f * sin(ang);
		AvionY -= 0.05f * cos(ang);
		fuel -= 0.01;
		glutPostRedisplay();
	}
	if (keyStates['a'] || keyStates['A']) {
		AvionAng += 0.02f;
		glutPostRedisplay();
	}
	if (keyStates['d'] || keyStates['D']) {
		AvionAng -= 0.02f;
		glutPostRedisplay();
	}
	if (keyStates['e'] || keyStates['E']) {
		if (DesplazamientoZ < 0.5) {
			DesplazamientoZ += 0.001;
			glutPostRedisplay();
		}
	}
	if (keyStates['q'] || keyStates['Q']) {
		if (DesplazamientoZ > -0.1) {				
			DesplazamientoZ -= 0.001;
			glutPostRedisplay();
		}
	}
};

// ------------------------ Callbacks ------------------------ //
// Arma un un nuevo buffer (back) y reemplaza el framebuffer
void Display_cb() {
	keyOperations();
	// Arma el back-buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Rellena con color de fondo
	DibujarPiso();
	DibujarPared();
	DibujaTecho();
	DibujarAvion();
	DibujarNube();
	DibujaStats();
	DibujaXYZ();
	glutSwapBuffers();									// Lo manda al monitor
  // Chequea errores
  int errornum=glGetError();
  while(errornum!=GL_NO_ERROR){
    if (cl_info){
      if(errornum==GL_INVALID_ENUM)
        cout << "GL_INVALID_ENUM" << endl;
      else if(errornum==GL_INVALID_VALUE)
        cout << "GL_INVALID_VALUE" << endl;
      else if (errornum==GL_INVALID_OPERATION)
        cout << "GL_INVALID_OPERATION" << endl;
      else if (errornum==GL_STACK_OVERFLOW)
        cout << "GL_STACK_OVERFLOW" << endl;
      else if (errornum==GL_STACK_UNDERFLOW)
        cout << "GL_STACK_UNDERFLOW" << endl;
      else if (errornum==GL_OUT_OF_MEMORY)
        cout << "GL_OUT_OF_MEMORY" << endl;
    }
    errornum=glGetError();
  }
};

// Ventana
void Reshape_cb(int width, int height){					// Maneja cambios de ancho y alto de la ventana
  if (!width||!height) return;							// minimizado ==> nada
  w=width; h=height;
  glViewport(0,0,w,h);									// regi�n donde se dibuja (toda la ventana)
  glMatrixMode(GL_PROJECTION);  glLoadIdentity();		// rehace la matriz de proyecci�n (la porcion de espacio visible)
  glOrtho(0,w,0,h,-1,1);								// unidades = pixeles
  glMatrixMode(GL_MODELVIEW);							// Las operaciones subsiguientes se aplican a la matriz de modelado GL_MODELVIEW
  glutPostRedisplay();									// Avisa que se debe redibujar
};

// Idle para el tiempo
void idle_cb() {
	static unsigned int lt = 0;
	int dt = glutGet(GLUT_ELAPSED_TIME) - lt;
	if (dt > 60) {
		lt = glutGet(GLUT_ELAPSED_TIME);
		glutPostRedisplay();
	}
};

// Special keys (non-ASCII)
// teclas de funcion, flechas, page up/dn, home/end, insert
void Special_cb(int key,int xm=0,int ym=0) {
  if (key==GLUT_KEY_F4 && glutGetModifiers()==GLUT_ACTIVE_ALT) // alt+f4 => exit
    exit(EXIT_SUCCESS);
}

void inicializa() {
  // GLUT
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);	// pide color RGB, double buffering, y profundiad
	glutInitWindowSize(w,h); glutInitWindowPosition(100, 100);
	glutCreateWindow("Ejemplo Avion"); // crea el main window
	// declara los callbacks, los que (aun) no se usan (aun) no se declaran
	glutDisplayFunc(Display_cb);
	glutReshapeFunc(Reshape_cb);
	//glutKeyboardFunc(Keyboard_cb);
	//keyOperations();
	// glutKeyboardFunc(keyOperations);
	glutKeyboardFunc(keyPressed);						// Tell GLUT to use the method "keyPressed" for key presses  
	glutKeyboardUpFunc(keyUp);						// Tell GLUT to use the method "keyUp" for key up events
	glutSpecialFunc(Special_cb);
	glutIdleFunc(idle_cb);
	// OpenGL
	glClearColor(0.01f,0.01f,0.01f,1.f);
	// Activa el uso del Z-Buffer (Depth)
	glEnable(GL_DEPTH_TEST);							// Habilita el z-buffer
	glDepthFunc(GL_LESS);								// Funci�n de comparaci�n de z


	//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(2, texid);
  
	// Textura 2D
	// Textura de pasto
	glBindTexture(GL_TEXTURE_2D, texid[0]);
	mipmap_ppm("grass2.ppm");
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glEnable(GL_TEXTURE_2D);

	// Textura nube
	//glGenTextures(1, &texNube);
	glBindTexture(GL_TEXTURE_2D, texid[1]);
	mipmap_ppm("cloud.ppm");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glEnable(GL_TEXTURE_2D);

  // las operaciones subsiguientes se aplican a la matriz de modelado GL_MODELVIEW
  glMatrixMode(GL_MODELVIEW); glLoadIdentity();
}

int main(int argc,char** argv) {
  // teclas a utilizar
  cout << "Teclas a utilizar:" << endl;
  cout << "w: avanza" << endl;
  cout << "s: retrocede" << endl;
  cout << "d: gira en sentido horario" << endl;
  cout << "a: gira en sentido antihorario" << endl;
  cout << "q: envia al fondo" << endl;
  cout << "e: trae hacia adelante" << endl;
  
  glutInit(&argc,argv); // inicializaci�n interna de GLUT
  inicializa(); // define el estado inicial de GLUT y OpenGL
  glutMainLoop(); // entra en loop de reconocimiento de eventos
  return 0; // nunca se llega ac�, es s�lo para evitar un warning
};
